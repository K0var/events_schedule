#include "../inc/screen.h"

void Screen_PrepareScreen (struct Screen *myScreen)
{
    myScreen->currentEvent = Event_CreateEmptyEvent();
    myScreen->fiveNextEvents = EventList_CreateEmptyList();
    for (int i=0; i<5; ++i) EventList_pushBack(myScreen->fiveNextEvents, Event_CreateEmptyEvent());
    myScreen->laterEvents = EventList_CreateEmptyList();
}

void Screen_FreeContent (struct Screen *myScreen)
{
    Event_DestroyEvent(myScreen->currentEvent);
    EventList_destroyEventList(myScreen->fiveNextEvents);
    free(myScreen->fiveNextEvents);
    EventList_destroyEventList(myScreen->laterEvents);
    free(myScreen->laterEvents);

    myScreen->currentEvent = NULL;
    myScreen->fiveNextEvents = NULL;
    myScreen->laterEvents = NULL;
}

void Screen_ShowContent (struct Screen *myScreen)
{
    printf("\nCurrent:\n\n");
    Event_ShowEventContent(myScreen->currentEvent);

    printf("\nnext\n\n");
    iEventListElement *mylistElement = EventList_GetListBegin(myScreen->fiveNextEvents);
    iEvent *ptr = EventList_GetEventElementFromEventListElement(mylistElement);
    Event_ShowEventContent(ptr);
    while (mylistElement != EventList_GetListEnd(myScreen->fiveNextEvents))
    {
        mylistElement = EventList_GetNextEventListElement(mylistElement);
        ptr = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(ptr);
    }

    mylistElement = EventList_GetListBegin(myScreen->laterEvents);
    if(mylistElement != NULL)
    {
        printf("\nnext next\n\n");
        ptr = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(ptr);
        while (mylistElement != EventList_GetListEnd(myScreen->laterEvents))
        {
            mylistElement = EventList_GetNextEventListElement(mylistElement);
            ptr = EventList_GetEventElementFromEventListElement(mylistElement);
            Event_ShowEventContent(ptr);
        }
    }
}

void Screen_ModifyEvent_SetReserved (struct Screen *myScreen, const int eventNumber, const char *topic, const char *reserved_by, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes)
{
    if (eventNumber == 0) Event_SetReservedEvent(myScreen->currentEvent, topic, reserved_by, start_hour, start_minute, length_in_minutes);

    else if (eventNumber > 0)
    {
        iEventListElement *mylistElement;
        iEvent *ptr;
        if (eventNumber < 6)
        {
            mylistElement = EventList_GetListBegin(myScreen->fiveNextEvents);
            for (int i=1; i<eventNumber; ++i) mylistElement = EventList_GetNextEventListElement(mylistElement);
            ptr = EventList_GetEventElementFromEventListElement(mylistElement);
            Event_SetReservedEvent(ptr, topic, reserved_by, start_hour, start_minute, length_in_minutes);
        }
        else
        {
            EventList_pushBack(myScreen->laterEvents, Event_CreateEmptyEvent());
            mylistElement = EventList_GetListBegin(myScreen->laterEvents);
            while (mylistElement != EventList_GetListEnd(myScreen->laterEvents)) mylistElement = EventList_GetNextEventListElement(mylistElement);
            ptr = EventList_GetEventElementFromEventListElement(mylistElement);
            Event_SetReservedEvent(ptr, topic, reserved_by, start_hour, start_minute, length_in_minutes);
        }
    }
}

void Screen_ModifyEvent_SetUneserved (struct Screen *myScreen, const int eventNumber, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes)
{
    if (eventNumber == 0) Event_SetUnreservedEvent (myScreen->currentEvent, start_hour, start_minute, length_in_minutes);

    else if (eventNumber > 0 && eventNumber <= 5)
    {
        iEventListElement *mylistElement;
        iEvent *ptr;
        mylistElement = EventList_GetListBegin(myScreen->fiveNextEvents);
        for (int i=1; i<eventNumber; ++i) mylistElement = EventList_GetNextEventListElement(mylistElement);
        ptr = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_SetUnreservedEvent(ptr, start_hour, start_minute, length_in_minutes);
    }
}
