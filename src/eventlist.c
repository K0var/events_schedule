#include "../inc/eventlist.h"

struct EventListElement
{
    struct EventListElement *next;
    struct Event *EventElement;
};

static struct EventListElement *EventList_CreateEventListElement (struct Event *eventElelement)
{
    struct EventListElement *new_element = malloc(sizeof(*new_element));
    if (new_element != NULL)
    {
        new_element->EventElement=eventElelement;
        new_element->next=NULL;
    }
    return new_element;
}

struct EventListElement *EventList_GetNextEventListElement (struct EventListElement *currentELE)
{
    assert(currentELE->next != NULL);
    return currentELE->next;
}

struct Event *EventList_GetEventElementFromEventListElement (struct EventListElement *ELE)
{
    assert(ELE->EventElement != NULL);
    return ELE->EventElement;
}

struct EventList
{
    struct EventListElement *listBegin;
    struct EventListElement *listEnd;
};

struct EventList *EventList_CreateEmptyList (void)
{
    struct EventList *new_list = malloc(sizeof(*new_list));
    if (new_list != NULL)
    {
        new_list->listBegin=NULL;
        new_list->listEnd=NULL;
    }
    return new_list;
}

struct EventListElement *EventList_GetListBegin (struct EventList *mylist)
{
    //assert(mylist->listBegin != NULL);
    return mylist->listBegin;
}

struct EventListElement *EventList_GetListEnd (struct EventList *mylist)
{
    assert(mylist->listEnd != NULL);
    return mylist->listEnd;
}

void EventList_pushBack(struct EventList *eList, struct Event *E)
{
    struct EventListElement *ptr = NULL;
    struct EventListElement *newElement = EventList_CreateEventListElement(E);
    if (eList->listBegin==NULL)
    {
        eList->listBegin=newElement;
        eList->listEnd=newElement;
    }
    else
    {
        ptr = eList->listBegin;
        while (ptr->next != NULL) ptr = ptr->next;
        ptr->next = newElement;
        eList->listEnd = ptr->next;
    }

}

void EventList_destroyEventListElement(struct EventListElement *ELE, struct EventList *List)
{
    struct EventListElement *ptr = List->listBegin;

    if (ELE == List->listBegin)
    {
        List->listBegin = ptr->next;
        if (ELE == List->listEnd) List->listEnd = ptr->next;
        Event_DestroyEvent(EventList_GetEventElementFromEventListElement(ELE));
        free(ELE);
        ELE = NULL;
        //ptr = NULL;
    }

    else
    {
        while(ptr->next != ELE && ptr->next != NULL) ptr = ptr->next;

        struct EventListElement *erased = ptr->next;
        ptr->next = erased->next;
        if (ELE == List->listEnd) List->listEnd = ptr;
        Event_DestroyEvent(EventList_GetEventElementFromEventListElement(erased));
        free(erased);
        ELE = NULL;
    }

}

void EventList_destroyEventList(struct EventList *List)
{
    while (List->listBegin != NULL)
        EventList_destroyEventListElement(List->listBegin, List);
}
