#include "../inc/event.h"

typedef enum event_status {RESERVED, UNRESERVED} event_status;

struct Event
{
    event_status status;
    char *topic;
    char *reserved_by;
    uint8_t start_hour;
    uint8_t start_minute;
    uint16_t length_in_minutes;
};

void Event_DestroyEvent (struct Event *E)
 {
   //assert (E != NULL);
   free(E->topic);
   free(E->reserved_by);
   free(E);
 }

struct Event *Event_CreateEmptyEvent (void)
{
    struct Event *new_event = malloc(sizeof(*new_event));
    if (new_event != NULL)
    {
        new_event->status = UNRESERVED;
        new_event->topic = NULL;
        new_event->reserved_by = NULL;
        new_event->start_hour = 0;
        new_event->start_minute = 0;
        new_event->length_in_minutes = 0;
    }
    return new_event;
}

void Event_SetUnreservedEvent (struct Event *E_unreserved, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes)
{
    const char *topic="TERAZ WOLNE";

    E_unreserved->status = UNRESERVED;

    free(E_unreserved->topic);
    E_unreserved->topic = malloc(strlen(topic)+1);
    if (E_unreserved->topic != NULL) strcpy(E_unreserved->topic, topic);

    free(E_unreserved->reserved_by);
    E_unreserved->reserved_by = NULL;

    E_unreserved->start_hour = start_hour;
    E_unreserved->start_minute = start_minute;
    E_unreserved->length_in_minutes = length_in_minutes;
}

void Event_SetReservedEvent (struct Event *E_reserved, const char *topic, const char *reserved_by, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes)
{
    E_reserved->status = RESERVED;

    free(E_reserved->topic);
    E_reserved->topic = malloc(strlen(topic)+1);
    if (E_reserved->topic != NULL) strcpy(E_reserved->topic, topic);

    free(E_reserved->reserved_by);
    E_reserved->reserved_by = malloc(strlen(reserved_by)+1);
    if (E_reserved->reserved_by != NULL) strcpy(E_reserved->reserved_by, reserved_by);

    E_reserved->start_hour = start_hour;
    E_reserved->start_minute = start_minute;
    E_reserved->length_in_minutes = length_in_minutes;
}

void Event_ShowEventContent (struct Event *E)
{
    if (E->status==UNRESERVED)
    {
        if (E->topic!=NULL) printf("\n%s", E->topic);
        printf("\nfrom %i:%i\n",E->start_hour, E->start_minute);
    }
    else
    {
        printf("\n%s",E->topic);
        printf("\nreserved by: %s",E->reserved_by);
        printf("\nfrom %i:%i\n",E->start_hour,E->start_minute);
    }
}

