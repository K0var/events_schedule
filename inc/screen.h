#ifndef SCREEN_H_INCLUDED
#define SCREEN_H_INCLUDED

#include "event.h"
#include "eventlist.h"



typedef struct Screen
{
    iEvent *currentEvent;
    iEventList *fiveNextEvents;
    iEventList *laterEvents;

} iScreen;

void Screen_PrepareScreen (struct Screen *myScreen);
void Screen_FreeContent (struct Screen *myScreen);
void Screen_ShowContent (struct Screen *myScreen);
void Screen_ModifyEvent_SetReserved (struct Screen *myScreen, const int eventNumber, const char *topic, const char *reserved_by, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes);
void Screen_ModifyEvent_SetUneserved (struct Screen *myScreen, const int eventNumber, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes);

#endif // SCREEN_H_INCLUDED
