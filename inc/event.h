#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

typedef struct Event iEvent;

void Event_DestroyEvent (struct Event *E);
struct Event *Event_CreateEmptyEvent (void);
void Event_SetUnreservedEvent (struct Event *E_unreserved, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes);
void Event_SetReservedEvent (struct Event *E_reserved, const char *topic, const char *reserved_by, const uint8_t start_hour, const uint8_t start_minute, const uint16_t length_in_minutes);
void Event_ShowEventContent (struct Event *E);


#endif // EVENT_H_INCLUDED
