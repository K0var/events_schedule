#ifndef EventList_H_INCLUDED
#define EventList_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "event.h"

typedef struct EventListElement iEventListElement;

typedef struct EventList iEventList;

struct EventListElement *EventList_GetNextEventListElement (struct EventListElement *currentELE);
struct Event *EventList_GetEventElementFromEventListElement (struct EventListElement *ELE);

struct EventList *EventList_CreateEmptyList (void);
struct EventListElement *EventList_GetListBegin (struct EventList *mylist);
struct EventListElement *EventList_GetListEnd (struct EventList *mylist);
void EventList_pushBack(struct EventList *eList, struct Event *E);
//struct Event *EventList_getEvent(struct EventList *eList,const int elementNumber);
void EventList_destroyEventListElement(struct EventListElement *ELE, struct EventList *List);
void EventList_destroyEventList(struct EventList *List);


#endif // EventList_H_INCLUDED
