#include <stdio.h>
#include <stdlib.h>
#include "inc/event.h"
#include "inc/eventlist.h"
#include "inc/screen.h"

int main()
{
    printf("Hello world!\n");

    iEvent *firstEvent=Event_CreateEmptyEvent();
    Event_ShowEventContent(firstEvent);
    Event_SetUnreservedEvent(firstEvent, 10, 12, 30);
    Event_ShowEventContent(firstEvent);
    Event_SetReservedEvent(firstEvent,"Will be funny", "Rafal", 12, 0, 30);
    Event_ShowEventContent(firstEvent);
    Event_DestroyEvent(firstEvent);
    firstEvent=NULL;

    iEventList *mylist=EventList_CreateEmptyList();

    for(int i=1; i<=5; ++i)
    {
        char topic[20];
        sprintf(topic,"topic %i", i);
        iEvent *wsk=Event_CreateEmptyEvent();
        Event_SetReservedEvent(wsk,topic,"Rafal",i,0,30);
        EventList_pushBack(mylist,wsk);
    }

    // Here I am printing content of mylist

    iEventListElement *mylistElement = EventList_GetListBegin(mylist);
    iEvent *wsk = EventList_GetEventElementFromEventListElement(mylistElement);
    Event_ShowEventContent(wsk);
    while (mylistElement != EventList_GetListEnd(mylist))
    {
        mylistElement = EventList_GetNextEventListElement(mylistElement);
        wsk = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(wsk);
    }
    //mylistElement = EventList_GetNextEventListElement(mylistElement); there will be an assert

    // Here I destroyed First element of mylist and I am printing content of mylist

    mylistElement = EventList_GetListBegin(mylist);
    EventList_destroyEventListElement(mylistElement, mylist);

    mylistElement = EventList_GetListBegin(mylist);
    wsk = EventList_GetEventElementFromEventListElement(mylistElement);
    Event_ShowEventContent(wsk);
    while (mylistElement != EventList_GetListEnd(mylist))
    {
        mylistElement = EventList_GetNextEventListElement(mylistElement);
        wsk = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(wsk);
    }

    // Here I destroyed Last element of mylist and I am printing content of mylist

    mylistElement = EventList_GetListEnd(mylist);
    EventList_destroyEventListElement(mylistElement, mylist);

    mylistElement = EventList_GetListBegin(mylist);
    wsk = EventList_GetEventElementFromEventListElement(mylistElement);
    Event_ShowEventContent(wsk);
    while (mylistElement != EventList_GetListEnd(mylist))
    {
        mylistElement = EventList_GetNextEventListElement(mylistElement);
        wsk = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(wsk);
    }

    // Here I am destroying mylist and creating new content

    EventList_destroyEventList(mylist);
    free(mylist);

    mylist=EventList_CreateEmptyList();

    for(int i=1; i<=5; ++i)
    {
        char topic[20];
        sprintf(topic,"new_topic %i", i);
        iEvent *wsk=Event_CreateEmptyEvent();
        Event_SetReservedEvent(wsk,topic,"new_Rafal",i,0,30);
        EventList_pushBack(mylist,wsk);
    }

    // Here I am printing content of mylist

    mylistElement = EventList_GetListBegin(mylist);
    wsk = EventList_GetEventElementFromEventListElement(mylistElement);
    Event_ShowEventContent(wsk);
    while (mylistElement != EventList_GetListEnd(mylist))
    {
        mylistElement = EventList_GetNextEventListElement(mylistElement);
        wsk = EventList_GetEventElementFromEventListElement(mylistElement);
        Event_ShowEventContent(wsk);
    }

    EventList_destroyEventList(mylist);
    free(mylist);

    iScreen screen;
    Screen_PrepareScreen(&screen);
    Screen_ShowContent(&screen);
    Screen_ModifyEvent_SetReserved(&screen, 0, "something", "someone", 12, 30, 50);
    Screen_ShowContent(&screen);
    Screen_ModifyEvent_SetReserved(&screen, 6, "something", "someone", 12, 30, 50);
    Screen_ShowContent(&screen);
    Screen_ModifyEvent_SetReserved(&screen, 4, "something", "someone", 12, 30, 50);
    Screen_ShowContent(&screen);
    Screen_ModifyEvent_SetUneserved(&screen, 4, 12, 30, 50);
    Screen_ShowContent(&screen);
    Screen_ModifyEvent_SetUneserved(&screen, 0, 12, 30, 50);
    Screen_ShowContent(&screen);

    Screen_FreeContent(&screen);

    return 0;
}
